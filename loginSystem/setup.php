<?php

if (!$all) { require "allFiles.php"; } #Include Functions if they're not already here

$ban = #Ban table: Stores basic user information about each user that is banned. Enough to keep them off the site
mysql_query("

CREATE TABLE `".$database_preface."ban` (
	`id` INT(50) NOT NULL AUTO_INCREMENT,
	`date` VARCHAR(50) NULL DEFAULT NULL,
	`ip` VARCHAR(50) NULL DEFAULT NULL,
	`username` VARCHAR(50) NULL DEFAULT NULL,
	`issuer` VARCHAR(50) NOT NULL DEFAULT 'No issuer provided.',
	`reason` VARCHAR(512) NOT NULL DEFAULT 'No reason provided.',
	`appealed` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
AUTO_INCREMENT=0;

");

$userblobs = #Userblobs table: Attaches a long session code to a username and a date to base expiration dates off of, as well as storing an action to differentiate codes and sessions
mysql_query("

CREATE TABLE `".$database_preface."userblobs` (
	`id` INT(5) NOT NULL AUTO_INCREMENT,
	`user` VARCHAR(100) NOT NULL,
	`code` VARCHAR(256) NOT NULL,
	`action` VARCHAR(100) NOT NULL,
	`date` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
AUTO_INCREMENT=0;

");

$users = #Users table: Stores all information about users
mysql_query("

CREATE TABLE `".$database_preface."users` (
	`id` INT(255) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(50) NOT NULL,
	`oldusername` VARCHAR(50) NOT NULL,
	`permusername` VARCHAR(50) NOT NULL,
	`name_first` VARCHAR(256) NOT NULL,
	`name_last` VARCHAR(256) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	`oldpassword` VARCHAR(100) NOT NULL,
	`passwordchanged` VARCHAR(50) NOT NULL DEFAULT '0000000000',
	`salt` VARCHAR(512) NOT NULL,
	`oldsalt` VARCHAR(512) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`oldemail` VARCHAR(512) NOT NULL,
	`emailchanged` VARCHAR(64) NOT NULL DEFAULT '0000000000',
	`firstemail` VARCHAR(512) NOT NULL,
	`date_registered` VARCHAR(255) NOT NULL,
	`activated` INT(1) NOT NULL DEFAULT '0',
	`bio` VARCHAR(510) NOT NULL DEFAULT '',
	`sig` VARCHAR(100) NOT NULL DEFAULT '',
	`title` VARCHAR(50) NOT NULL DEFAULT '',
	`rep` INT(10) NOT NULL DEFAULT '0',
	`2step` INT(1) NOT NULL DEFAULT '0',
	`last_logged_in` VARCHAR(50) NOT NULL DEFAULT '0000000000',
	`old_last_logged_in` VARCHAR(50) NOT NULL DEFAULT '0000000000',
	`site` VARCHAR(255) NOT NULL,
	`ip` VARCHAR(64) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
AUTO_INCREMENT=0;

");

if ($ban) { #If the ban table was made successfully, let the user know
  echo $database['database'].'> Bans table made successfully ('.$database_preface.'bans)<br>';
} else {
  echo '<font style="color: red">'.$database['database'].'> Bans table NOT made successfully (name may be taken, or you might not be connected to your database correctly).</font><br>';
}
if ($userblobs) { #If the userblobs table was made successfully, let the user know
  echo $database['database'].'> User blobs table made successfully ('.$database_preface.'userblobs)<br>';
} else {
  echo '<font style="color: red">'.$database['database'].'> User Blobs table NOT made successfully (name may be taken, or you might not be connected to your database correctly).</font><br>';
}
if ($users) { #If the users table was made successfully, let the user know
  echo $database['database'].'> Users table made successfully ('.$database_preface.'users)<br>';
} else {
  echo '<font style="color: red">'.$database['database'].'> Users table NOT made successfully (name may be taken, or you might not be connected to your database correctly).</font><br>';
}

?>
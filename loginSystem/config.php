<?php



$setup = true;

#Default: false

#About  : After setting up this file for your systems, mark this as true

#Example: if (!$setup) { echo $setup_forms; }



$database = array(

	"location" => "localhost", 

	"database" => "", 

	"username" => "username", 

	"password" => "password"

);

#Default: array("location" => "localhost", "database" => "", "username" => "username", "password" => "password");

#About  : The connection information for your MySQL database

#Example: See below

mysql_connect($database['location'], $database['username'], $database['password']) or die(mysql_error());

mysql_select_db($database['database']);



$database_preface = 'ls_';

#Default: "ls_"

#About  : The preface for database tables

#Example: mysql_query("UPDATE ".$database_preface." SET bla='blah'");



$sitename = "examplecom";

#Default: "examplecom"

#About  : The name of the website (the best being the domain name without a period)

#Example: $_COOKIE[$sitename]



$domain_simple = "example.com";

#Default: "example.com"

#About  : The simple and master url of the entire website (NOTE: It is used for the cookies that are used to stay logged in, it should be the master url of your site so that all of your subdomains can access the cookie)

#Example: "Finish registering for '.$domain_simple.' now!"



$domain = "accounts.example.com";

#Default: "accounts.example.com"

#About  : The url of the root of your website

#Example: "Please click the following link: http://'.$domain.'/recover



$system_location = "/loginSystem";

#Default: "/loginSystem"

#About  : The folder of where all of the login system's files are located

#Example: <a href="'.$system_location.'/config.php"



$encryption = false;

#Default: false

#About  : Toggles whether AES-compliant encryption is on or off for emails and IPs (true or false only (booleans))

#Example: if ($encyption === true) { $email = encrypt($email) }



$editing_page = "loggedin.php";

#Default: "loggedin.php"

#About  : The page originally named loggedin.php that allows the user to edit their account information

#Example: <a href="'.$editing_page.'">Editing page</a>



$registration_page = "register.php";

#Default: "register.php"

#About  : The page originally named register.php that allows a user to register for an account

#Example: <a href="'.$registration_page.'">Register an Account</a>



$recovery_page = "recover.php";

#Default: "recovery.php"

#About  : The page originally named register.php that allows a user to register for an account

#Example: If this WAS you, please follow this link in order to reset your password: //{$domain}{$system_location}/{$recover_page}?blob={$b}

 

?>

###Use [Zbee/UserSystem](https://github.com/Zbee/UserSystem) instead

#loginSystem.PHP

A ready to deploy login system that is both secure, and simple.

It's not only easy and quick to deploy, but the simple, little system can also easily be used with other systems by me (Such as: my [Blog System](https://github.com/Zbee/blogSystem.PHP)).

##Security

* The hashing and transfer methods are all FIPS 140-2 Level 2 compliant (uses a DoD-approved hashing algorithm, tamper-checking on sessions, and removal of possibly tampered-with sessions).

* Uses a system I like to refer to as a blob system, which is basically thus: The cookie is simply a very long string (more than a googol of possibilities) that is then stored in the database and hard-coded to the user who owns the code. Furthermore, all blobs that can be are destroyed after use, and have expiration dates.

* Can easily be configured (through the simple config file) to use an AES-compliant encryption system for emails and IPs that are stored with the user.

* Has a very simple 2Step Authentication system that will delay login of users with it activated and require them to follow a link in an email sent to them, again utilizing the blob system.

* SSL (https://) connections as well as http:// connections are both supported.

* The logout file has conventions in place to allow for users to destroy every session tied to their account, increasing their ability to keep their own accounts as secure as possible.

* Rainbow and Lookup tables are halted in their tracks by use of very secure salts that are utilized in the hashing of user's passwords. These salts are recreated everytime the user changes their password. In addition to that, the salts also have nearly a googol of possibilities.

* SQL injection attacks are stopped upfront by sanitizing user input. They're also stopped with the second method of how the PHP and MySQL commands work together, which does not allow for multiple SQL queries to take place in the same PHP function, disabling an attacker from editing data, grabbing data, or removing data from a database.

##Simplicity

* I've included example pages for every account action necessary, removing any strenuous recreation of what a page should have on it.

* It's incredibly simple to fetch and display user information, all you have to do is validate the session and the needed data will be returned.

* All of the options for the login system are nicely wrapped up in a simple configuration file, which has very nice doumentation in it enabling anyone to use it.

* There is no code you have to create for yourself for the most part, it is all created with simplicity in mind.

* The database structure was also created with simplicity in mind, and leaves plenty of room for extra doo-dads.

* Stores valuable data that can be used in many other systems and is vital for many circumstances. Data such as IPs, one previous password, one previous email, one previous username, the first email, the first username, when they last logged on, when they last changed their password, when they last changed their email, and more!

##Todo

* Add documents on how to customize the system for your situation

* Add pictures of how the whole system should look

##License
Copyright (c) 2014 Ethan Henderson See the LICENSE file for license rights and limitations (MIT).
